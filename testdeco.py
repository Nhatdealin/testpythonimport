def mul(func):
    def x(a,b):
        r = func(a,b)
        return r*6
    return x

def add(a,b):
    return a+b

def minus(a,b):
    return a-b

if(__name__ == "__main__"):
   print( mul(add)(3,4))